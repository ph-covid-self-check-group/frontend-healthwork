module.exports = {
    pwa: {
        workboxOptions: {
            skipWaiting: true
        },
        themeColor: '#0D47A1',
    },
    devServer: {
        disableHostCheck: true
    }
}