---
title: "Testing"
date: 2020-03-03
draft: false
weight: 40
---

To run the unit/integration/e2e test cases

```bash
npm test
```
