import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './style/main.sass'
import {MenuIcon, ArrowLeftIcon, EyeIcon, CheckIcon, LogOutIcon, AlertTriangleIcon, CodeIcon, ChevronDownIcon, ChevronUpIcon, XIcon} from 'vue-feather-icons'
import Tabs from './components/Tabs'
import axios from 'axios'
import {firebase, ui} from '@/modules/firebase.js'
import {timeoutInMinutes} from './config.js'

Vue.component('MenuIcon', MenuIcon)
Vue.component('ArrowLeftIcon', ArrowLeftIcon)
Vue.component('LogOutIcon', LogOutIcon)
Vue.component('EyeIcon', EyeIcon)
Vue.component('Tabs', Tabs)
Vue.component('CheckIcon', CheckIcon)
Vue.component('AlertTriangleIcon', AlertTriangleIcon)
Vue.component('ChevronDownIcon',ChevronDownIcon)
Vue.component('ChevronUpIcon', ChevronUpIcon)
Vue.component('CodeIcon', CodeIcon)
Vue.component('XIcon', XIcon)

Vue.config.productionTip = false

const Sentry = require('@sentry/browser');
Sentry.init({ dsn: 'https://114e52e31fec41f1879c864e4153b757@sentry.io/5171144' });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

var timeoutPadding = timeoutInMinutes * 60 * 1000; //in milliseconds
var timeoutHandler = null;

//axios request interceptor
axios.interceptors.request.use(async(config)=>{
  let token = await refreshToken();
  if(token != "")
  {
    let newAuthorization = `Bearer ${token}`;
    if(newAuthorization != config.headers.Authorization)
    {
      config.headers.Authorization =  newAuthorization;
      store.commit('setToken', token);
    }
  }
  sessionTimeoutRunner();
  store.commit('showLoader')
  return config;
}, error => {
  store.commit('hideLoader')
  return Promise.reject(error);
});

//axios response interceptor
axios.interceptors.response.use(response => {
  store.commit('hideLoader')
  return response;
  },
  error => {
  if (error.response.status === 401) {
    store.commit('hideLoader')
    router.push({ name: 'Unauthorized' });
  }
  return error;
});

var firebaseUser = null;
firebase.auth().onAuthStateChanged(async(user) => {
  if (user) {
    firebaseUser = user;
  } else {
    firebaseUser = null;
  }
});

async function refreshToken(){
  try {
    if(firebaseUser)
    {
      const token = await firebaseUser.getIdToken(true);
      return token;
    }
    else
    {
      return "";
    }
  } catch(error) {
    return "";
  }
}

//session timeout
function sessionTimeoutRunner()
{
  window.clearTimeout(timeoutHandler);
  timeoutHandler = window.setTimeout(function(){
    sessionTimeout()
  }, timeoutPadding);
}

function sessionTimeout()
{
  firebaseUser = null;
  if(router.currentRoute.name != "Auth")
  {
    firebase.auth().signOut();
    store.commit('setToken', null);
    store.commit('setUser', null)
    store.commit('setPatients', [])
    store.commit('setAdmin', null)
    router.push({ name: 'Unauthorized' });
  }
}