import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Patient from '../views/Patient.vue'
import Auth from '../views/Auth.vue'
import $store from '../store/index'
import axios from 'axios'
import Unauthorized from '../views/Unauthorized'
import {backendURL} from '../config.js'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      authRequired: true,
      skipUnauthorizedRoute: true
    }
  },
  {
    path: '/auth',
    name: 'Auth',
    component: Auth
  },
  {
    path: '/users',
    name: 'Users',
    component: () => import('../views/Users'),
    meta: {
      adminRequired: true
    }
  },
  {
    path: '/open',
    name: 'Open',
    component: Home,
    meta: {
      tokenRequired: true
    }
  },
  {
    path: '/unauthorized',
    name: 'Unauthorized',
    component: Unauthorized
  },
  {
    path: '/closed',
    name: 'Closed',
    component: Home,
    meta: {
      tokenRequired: true
    }
  },
  {
    path: '/:patient',
    name: 'Patient',
    component: Home,
    meta: {
      tokenRequired: true
    }
  },
  
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {

  setTimeout(()=> {
    $store.commit('setNotif', null)
  }, 4000)

  if(to.meta.tokenRequired){
    var token = $store.state.auth.token
    if(token){
      next()
    }
    else {
      next({name: 'Auth'})
    }
  }

  if(to.meta.adminRequired){
    var token = $store.state.auth.token
    if(token){
      axios.get(`${backendURL}/auth/admin`, { headers: { Authorization: `Bearer ${token}` } }).then(response => {
        if(response.data.success){
          next()
        }
        else {
          next({name: 'Unauthorized'})
        }
        }).catch(error => {
          next({name: 'Unauthorized'})
        })
    }
    else {
      next({name: 'Auth'})
    }
  }
  
  else if(to.meta.authRequired){
    var token = $store.state.auth.token

    if(token){
      
      axios.get(`${backendURL}/auth`, { headers: { Authorization: `Bearer ${token}` } }).then(response => {
        if(response.data.success){
          next()
        }
        else {
        
          $store.commit('setUser', null)
          $store.commit('setToken', null)

          if(to.meta.skipUnauthorizedRoute)
            next({name: 'Auth'})
          else
            next({name: 'Unauthorized'})
        }
      })
      .catch(error => {
        $store.commit('setUser', null)
        $store.commit('setToken', null)

        if(to.meta.skipUnauthorizedRoute)
          next({name: 'Auth'})
        else
          next({name: 'Unauthorized'})
      })
      
    }
    else {
      next({name: 'Auth'})
    }
  }
  else{
    next()
  }
  
 
})


export default router
