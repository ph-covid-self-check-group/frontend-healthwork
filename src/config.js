var firebaseConfig = {
    apiKey: "AIzaSyB4yZTq_JXgV_zr2J38YFAsEeomsP5LY64",
    authDomain: "ph-covid-self-check-dev.firebaseapp.com",
    databaseURL: "https://ph-covid-self-check-dev.firebaseio.com",
    projectId: "ph-covid-self-check-dev",
    storageBucket: "ph-covid-self-check-dev.appspot.com",
    messagingSenderId: "5517847506",
    appId: "1:5517847506:web:57358553e6ca9447b4760b",
    measurementId: "G-SC3ZP7C75G"
  };

var backendURL = 'https://backend-serverless-vkupy7dkca-ue.a.run.app'

var timeoutInMinutes = 5; // in minutes

export {firebaseConfig, backendURL, timeoutInMinutes}