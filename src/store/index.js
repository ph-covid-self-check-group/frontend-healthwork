import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist';
import patient from './modules/patient.js'
import users from './modules/users.js'
import localization from './modules/localization.js'
import auth from './modules/auth.js'


Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
  key: 'vuex', // The key to store the state on in the storage provider.
  storage: window.localStorage, // or window.sessionStorage or localForage
  // Function that passes the state and returns the state with only the objects you want to store.
  // reducer: state => state,
  // Function that passes a mutation and lets you decide if it should update the state in localStorage.
  // filter: mutation => (true)
})

export default new Vuex.Store({
  plugins: [vuexLocalStorage.plugin],
  state: {
    notification: null,
    loader: false
  },
  mutations: {
    setNotif(state, payload){
      state.notification = payload
    },
    showLoader(state){
      state.loader = true
    },
    hideLoader(state){
      state.loader = false
    }
  },
  actions: {
    createNotif(context, payload){
      context.commit('setNotif', payload)

      setTimeout(()=>{
        context.commit('setNotif', null)
      }, 5000)
    }
  },
  modules: {
    auth,
    patient,
    users,
    localization
  }
})
