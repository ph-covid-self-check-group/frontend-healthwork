export default {
    state: {
        language: 'en'
    },
    mutations: {
        setLanguage(state, payload){
            state.language = payload
        }
    },
    getters: {
        strCasesForReview: (state) => {
            switch (state.language) {
                case 'en': return 'Cases For Review' ; break;
                case 'ph': return 'Mga Kasong Dapat I-Rebyu' ; break;
            }
        },
        strReviewedCases: (state) => {
            switch (state.language) {
                case 'en': return 'Reviewed Cases' ; break;
                case 'ph': return 'Na-Rebyu Nang Mga kaso' ; break;
            }
        },
        strCaseNumber: (state) => {
            switch (state.language) {
                case 'en': return 'Case #' ; break;
                case 'ph': return 'Kaso #' ; break;
            }
        },
        strDateSubmitted: (state) => {
            switch (state.language) {
                case 'en': return 'Date Submitted' ; break;
                case 'ph': return 'Petsa' ; break;
            }
        },
        strAge: (state) => {
            switch (state.language) {
                case 'en': return 'Age' ; break;
                case 'ph': return 'Edad' ; break;
            }
        },
        strGender: (state) => {
            switch (state.language) {
                case 'en': return 'Gender' ; break;
                case 'ph': return 'Kasarian' ; break;
            }
        },
        strCaseStatus: (state) => {
            switch (state.language) {
                case 'en': return 'Case Status' ; break;
                case 'ph': return 'Katayuan' ; break;
            }
        },
        strAssessment: (state) => {
            switch (state.language) {
                case 'en': return 'Assessment' ; break;
                case 'ph': return 'Pagtatasa' ; break;
            }
        },
        strForReview: (state) => {
            switch (state.language) {
                case 'en': return 'For Review' ; break;
                case 'ph': return 'Kailangan ng Rebyu' ; break;
            }
        },
        strReviewed: (state) => {
            switch (state.language) {
                case 'en': return 'Reviewed' ; break;
                case 'ph': return 'Na-rebyu na' ; break;
            }
        },
    }
}