import axios from 'axios'
import router from '../../router/index'
import {backendURL} from '../../config.js'

export default {
    state: {
        patients: [],
        forReviewCount: 0,
        caseClosedCount: 0,
    },
    mutations: {
        setPatients(state, payload) {
            state.patients = payload
        },
        setForReviewCount(state, payload) {
            state.forReviewCount = payload
        },
        setCaseClosedCount(state, payload) {
            state.caseClosedCount = payload
        }
    },
    actions: {
        loadPatients(context, params) {
            var url = `${backendURL}/api/patient?`;
            if (params) {
                for (let key in params) {
                    if (params.hasOwnProperty(key)) {
                        url += `${key}=${params[key]}`
                    }
                    url += "&"
                }
            }

            axios
                .get(url, { headers: { Authorization: `Bearer ${context.rootState.auth.token}` } })
                .then(response => {
                    var result = response.data.items;

                    result = result.map(item => {

                        // Temporary Data
                        if (!item.caseClosed) {
                            item.caseClosed = false
                        }

                        // Temporary Data
                        if (!item.casestatus) {
                            item.casestatus = 'for-review'
                        }

                        if (item.actions[0]) {
                            if (item.actions[0] == "pui") {
                                item.severity = 'PUI'
                            } else if (item.actions[0] == "pum") {
                                item.severity = 'PUM'
                            } else {
                                item.severity = null
                            }
                        }

                        // // Virtual case number
                        // if(params.page)
                        // {
                        //     item.caseno = (params.page * 10) + result.indexOf(item) + 1
                        // }
                        // else
                        // {
                        //     item.caseno = result.indexOf(item) + 1
                        // }

                        return item

                    })

                    if (params.mutate) {
                        context.commit('setPatients', result)
                        if (params.caseClosed) {
                            context.commit('setCaseClosedCount', response.data.count)
                        }
                        else {
                            context.commit('setForReviewCount', response.data.count)
                        }
                    }

                    if ((params.mutateCloseCaseCounter) && (params.caseClosed)) {
                        context.commit('setCaseClosedCount', response.data.count)
                    }

                    if ((params.mutateForRevieCounter) && (!params.caseClosed)) {
                        context.commit('setForReviewCount', response.data.count);
                    }

                });
        },
        changeAssessment(context, config) {

            var newActions = config.oldActions
            newActions[0] = config.newAssessment

            axios
                .put(`${backendURL}/api/patient/${config.pid}`, {
                    data: {
                        actions: newActions
                        }
                    },
                    { headers: { Authorization: `Bearer ${context.rootState.auth.token}` } }

                )
                .then(response => {
                    if (response.data.success) {
                        context.dispatch('createNotif',
                            {
                                message: 'Patient has been updated',
                            })

                        setTimeout(() => {
                            context.dispatch('createNotif', null)
                            location.reload()
                        }, 1000)
                    }
                    else {
                        window.alert('Error: see log for details')
                        console.error(response.data)
                    }
                })
        },
        confirmRecommendation(context, pid) {


                var url = `${backendURL}/api/patient/${pid}`
                var body = {
                    data: {
                        caseClosed: true,
                        updatedAt: Date.now()
                    }
                }
                
                var bearerToken = `Bearer ${context.rootState.auth.token}`
                axios.put(url, { ...body }, { headers: { Authorization: bearerToken } })
                .then(response => {
                    if (response.data.success) {
                        router.push({ name: 'Closed' })
                        context.dispatch('createNotif',
                            {
                                message: `Case # ${pid} has been successfully reviewed and confirmed.`,
                                buttonText: 'View Case',
                                route: { name: 'Patient', params: { patient: pid } }
                            })
                    }
                    else {
                        window.alert('Error: see log for details')
                        console.error(response.data)
                    }
                })
        },
        reopenCase(context, pid) {

                var url = `${backendURL}/api/patient/${pid}`
                var body = {
                    data: {
                        caseClosed: false
                    }
                }
                var bearerToken = `Bearer ${context.rootState.auth.token}`
                axios.put(url, { ...body }, { headers: { Authorization: bearerToken } })
                .then(response => {
                    if (response.data.success) {
                        window.location.reload()
                    }
                    else {
                        window.alert('Error: see log for details')
                        console.error(response.data)
                    }
                })
        }
    },
}