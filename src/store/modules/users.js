import axios from 'axios'
import router from '../../router/index'
import {backendURL} from '../../config.js'

export default {
    state: {
        users: []
    },
    mutations: {
        setUsers(state, payload) {
            state.users = payload
        }
    },
    actions: {
        loadUsers(context, params) {
            var url = `${backendURL}/api/user?`;
            if(params)
            {
                for(let key in params)
                {
                    if(params.hasOwnProperty(key))
                    {
                        url += `${key}=${params[key]}`
                    }
                    url+="&"
                }
            }
            axios
                .get(url,{
                    headers : {
                        "Authorization" : `Bearer ${context.rootState.auth.token}`
                    }
                })
                .then(response => {
                    var result = response.data.items;

                    result = result.map(item => {

                        // Temporary Data
                        if (!item.fullname) {
                            item.fullname = "Sample user"
                        }
                        return item

                    })

                    if(params.mutate)
                    {
                        context.commit('setUsers', result)
                    }
                });
        },
        inviteUsers(context, params) {
            var url = `${backendURL}/api/user?`;
            axios
                .post(url,
                    params.data,
                    {
                        headers : {
                            "Authorization" : `Bearer ${context.rootState.auth.token}`
                        }
                    }
                )
                .then(response => {
                    var result = response.data.items;

                    // result = result.map(item => {

                    //     // Temporary
                    //     return item

                    // })

                    if(params.reload)
                    {
                        context.dispatch("loadUsers", {
                            mutate : true
                        });
                    }
                });
        },
        deleteUser(context, params) {
            var url = `${backendURL}/api/user/${params.uid}`;
            axios
                .delete(url,
                    {
                        headers : {
                            "Authorization" : `Bearer ${context.rootState.auth.token}`
                        }
                    }
                )
                .then(response => {
                    if(response.data.success)
                    {
                        if(params.reload)
                        {
                            context.dispatch("loadUsers", {
                                mutate : true
                            });
                        }
                    }
                    else
                    {
                        alert("Please try again later.")
                    }
                });
        },
        updateUser(context, params) {
            var url = `${backendURL}/api/user/${params.uid}`;
            axios
                .put(url,
                    {
                        data : {
                            fullname : params.fullname,
                            admin : params.admin
                        }
                    },
                    {
                        headers : {
                            "Authorization" : `Bearer ${context.rootState.auth.token}`
                        }
                    }
                )
                .then(response => {
                    if(response.data.success)
                    {
                        if(params.reload)
                        {
                            context.dispatch("loadUsers", {
                                mutate : true
                            });
                        }
                    }
                    else
                    {
                        alert("Please try again later.")
                    }
                });
        }
    }
}