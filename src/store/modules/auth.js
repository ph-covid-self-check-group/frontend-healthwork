import router from '../../router/index.js'

export default {
    state: {
        token: null,
        user: null,
        isAdmin: null // for ui only
    },
    mutations: {
        setToken(state, payload){
            state.token = payload
        },
        setUser(state, payload){
            state.user = payload
        },
        setAdmin(state, payload){
            state.isAdmin = payload
        },
    },
    actions: {
        logout(context){
            context.commit('setUser', null)
            context.commit('setToken', null)
            context.commit('setPatients', [])
            context.commit('setAdmin', null)
            router.push({name: 'Auth'})
        }
    }
}