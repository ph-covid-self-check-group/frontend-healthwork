import firebase from 'firebase/app'
import 'firebase/auth'
import store from '../store/index'
import {firebaseConfig} from '../config.js'
var firebaseui = require('firebaseui');



firebase.initializeApp(firebaseConfig);

var auth = firebase.auth();
var ui = new firebaseui.auth.AuthUI(auth)

// firebase.getCurrentUser = () => {
//   return new Promise((resolve, reject) => {
//       const unsubscribe = auth.onAuthStateChanged(user => {
//           unsubscribe();
//           // store.commit('setCurrentUser', user)
//           resolve(user);
//       }, reject);
//   })
// };

export { firebase, auth, ui };